import "../index.css";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import Dashboard from "../components/DashBoard";
import Layout from "../components/Layout"
 
function Home() {
  return (
    <Router>
      <Routes>
        <Route path="/" element={<Layout />}></Route>
        <Route index element={<Dashboard/>}></Route>
      </Routes>
    </Router>
  );
}

export default Home;
