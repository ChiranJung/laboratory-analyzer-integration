from django.urls import path, include
from rest_framework.routers import DefaultRouter
from .views import CreateUserView, MachineViewSet, TestTypeViewSet, TestResultViewSet

router = DefaultRouter()
router.register(r'machines', MachineViewSet)
router.register(r'testtypes', TestTypeViewSet)
router.register(r'testresults', TestResultViewSet)

urlpatterns = [
    path('users/', CreateUserView.as_view(), name='create_user'),
    path('', include(router.urls)),
]
