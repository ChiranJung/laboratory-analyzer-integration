from django.contrib.auth.models import User
from rest_framework import serializers
from .models import Machine, TestType, TestResult


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ["id", "username", "password"]
        extra_kwargs = {"password": {"write_only":True}}

    def create(self, validated_data):
        user = User.objects.create_user(**validated_data)
        return user
    
class MachineSerializer(serializers.ModelSerializer):
    class Meta:
        model = Machine
        fields = ['machine_id', 'machine_name', 'port_number', 'description', 'is_active']

class TestTypeSerializer(serializers.ModelSerializer):
    class Meta:
        model = TestType
        fields = ['test_type_id', 'test_type', 'machine_name', 'is_active']

class TestResultSerializer(serializers.ModelSerializer):
    class Meta:
        model = TestResult
        fields = ['test_result_id', 'sample_id', 'machine_name', 'raw_data', 'parsed_data', 'test_value', 'analysed_time', 'parsed_time']