from django.db import models

class Machine(models.Model):
    machine_id = models.AutoField(primary_key=True)
    machine_name = models.CharField(max_length=255, unique=True)
    port_number = models.IntegerField()
    description = models.CharField(max_length=255, blank=True, null=True)
    is_active = models.BooleanField(default=True)

    def __str__(self):
        return self.machine_name

class TestType(models.Model):
    test_type_id = models.AutoField(primary_key=True)
    test_type = models.CharField(max_length=255)
    machine_name = models.CharField(max_length=255)
    is_active = models.BooleanField(default=True)

    def __str__(self):
        return self.test_type

class TestResult(models.Model):
    test_result_id = models.AutoField(primary_key=True)
    sample_id = models.IntegerField()
    machine_name = models.CharField(max_length=255)
    raw_data = models.TextField()
    parsed_data = models.TextField()
    test_value = models.CharField(max_length=255)
    analysed_time = models.DateTimeField()
    parsed_time = models.DateTimeField()

    def __str__(self):
        return f"Sample {self.sample_id} - {self.test_value}"
