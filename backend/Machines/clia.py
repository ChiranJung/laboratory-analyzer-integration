import socket
import re
import os
import urllib.parse
import logging
from datetime import datetime
from tabulate import tabulate
import requests
import time


class Clia:
    def __init__(self):
        self.db_server = "10.9.0.11"
        self.url = "http://10.9.0.11/parse-datas.php"
        self.machine = 'hematology'
        self.host = '10.9.7.9'
        self.port = 5050
        self.EOT = b'\x04'
        self.ENQ = b'\x05'
        self.ACK = b'\x06'
        self.NAK = b'\x15'
        self.ETB = b'\x17'


class Status(Clia):
    def __init__(self):
        super().__init__()

    def check_server_status(self):
        response = os.system("ping -c 1 " + self.db_server)
        if response != 0:
            time.sleep(20)


class Server(Clia):
    def __init__(self):
        super().__init__()

    def connection(self):
        self.server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.server_socket.bind((self.host, self.port))
        self.server_socket.listen()

    def start(self):
        try:
            while True:
                self.client_socket, addr = self.server_socket.accept()
                self.handle_client(addr)
        except Exception as e:
            logging.error(f"Error in main loop: {e}")

    def handle_client(self, addr):
        connection = {datetime.now(): addr}
        tabl = [(key.strftime("%Y-%m-%d %H:%M:%S"), value)
                for key, value in connection.items()]
        print(tabulate(tabl, headers=["CON. TIME", "ANALYZER"], tablefmt="pretty"))
        to_parse = ''
        try:
            while True:
                data = self.client_socket.recv(2048)
                if data == self.ENQ:
                    to_parse = ''
                    self.client_socket.send(self.ACK)
                elif data == self.EOT:
                    to_parse += data.decode('utf-8', errors='ignore')
                    self.handle_data(to_parse)
                    to_parse = ""
                else:
                    to_parse += data.decode('utf-8', errors='ignore')
                    self.client_socket.send(self.ACK)
        except Exception as e:
            logging.error(f"Error handling client: {e}")
        finally:
            self.client_socket.close()

    def handle_data(self, to_parse):
        try:
            parsed_data = GetData.parse(self, to_parse)
            if parsed_data and GetData.dump_hmis(self, parsed_data):
                self.client_socket.send(self.ACK)
        except Exception as e:
            logging.error(f"Error handling data: {e}")


class GetData(Clia):
    def __init__(self):
        super().__init__()

    @staticmethod
    def parse(to_parse):
        try:
            sample = re.search(r"\^RACK\d[-]\d\|(.*?)\|", to_parse)
            if sample:
                sample_id = sample.group(1)

                T4 = re.search(r"FT4\^0\^F\|(.*?)\^\|ng\/dL", to_parse)
                FT4 = f"----FT4**{T4.group(1).strip('>''<')}" if T4 else 0

                T3 = re.search(r"FT3\^0\^F\|(.*?)\^\|pg\/mL", to_parse)
                FT3 = f"----FT3**{T3.group(1).strip('>''<')}" if T3 else 0

                TS = re.search(r'TSH\^0\^F\|(.*?)\^\|IU/mL', to_parse)
                TSH = f"----TSH**{TS.group(1).strip('>''<')}" if TS else 0

                Vit = re.search(r"VitD_1\^0\^F\|(.*?)\^\|ng\/mL", to_parse)
                VitD = f"----VitD**{Vit.group(1).strip('>''<')}" if Vit else 0

                VB12 = re.search(r'VB12(.*?)F\|(.*?)\^\|pg\/mL', to_parse)
                VB12II = f"----VB12II**{VB12.group(2).strip('>''<')}" if VB12 else 0

                dump_data = ''.join(filter(None, [sample_id, FT4, FT3, TSH, VitD, VB12II]))
                return dump_data
            return False
        except Exception as e:
            logging.error(f"Error parsing data: {e}")

    def dump_hmis(self, x):
        payload = {'machinename': 'hematology', 'dump': x}
        try:
            response = requests.get(self.url, params=urllib.parse.urlencode(payload))
            response.raise_for_status()
            if response.status_code == 200:
                with open(self.get_log_filename(), 'a') as f:
                    f.write(str(payload) + '\n' + '\n')
                data = {datetime.now(): x}
                table_data = [(key.strftime("%Y-%m-%d %H:%M:%S"), value) for key, value in data.items()]
                print(tabulate(table_data, headers=["TIME", "DATA"], tablefmt="fancy_grid"))
                return True
        except Exception as e:
            logging.error(f"Error dumping data to HMIS: {e}")
            return False

    @staticmethod
    def get_log_filename():
        now = datetime.now()
        date_str = now.strftime("%Y.%m.%d")
        log_folder = "logs/parsed_log"
        os.makedirs(log_folder, exist_ok=True)
        return f"{log_folder}/data_{date_str}.txt"


if __name__ == '__main__':
    logging.basicConfig(filename='app.log', level=logging.ERROR)
    status = Status()
    status.check_server_status()
    server = Server()
    server.connection()
    server.start()
